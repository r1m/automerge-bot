# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.4.1] 2020-05-18
### Added
- Success message on automatic merge retry success

## [0.4.0] 2020-05-15
### Added
- Workaround for GitLab Merge API regression

### Changed
- Build with Go 1.13
- Use AUTOMERGE_STABLE_BRANCH_PREFIX when fetching branches

## [0.3.0] 2019-08-14
### Added
- Attempt to self-approve the merge request

## [0.2.1] 2019-08-07
### Fixed
- Merge failures should not cause CI job to fail

## [0.2.0] 2019-08-05
### Added
- Option to stop automerge before reaching the default branch

## [0.1.0] 2019-06-06
### Added
- Link to original merge request or commit that started the merge train
- Assign original merge request author to automerge commits

## [0.0.2] 2019-05-10
### Added
- Automatically delete merged automerge branches and set to merge when pipeline succeeds

### Fixed
- Include certificates in the Docker container to prevent HTTPS API requests failing
- Automerge branch naming so that re-running the automerge script on the same job will not fail if the automerge branch for that commit already exists

## [0.0.1] 2019-05-10
### Added
- Minimal automated merge between stable branches.

[Unreleased]: https://gitlab.com/jramsay/automerge-bot/compare/v0.3.0...HEAD
[0.3.0]: https://gitlab.com/jramsay/automerge-bot/compare/v0.3.0...v0.2.1
[0.2.1]: https://gitlab.com/jramsay/automerge-bot/compare/v0.2.1...v0.2.0
[0.2.0]: https://gitlab.com/jramsay/automerge-bot/compare/v0.2.0...v0.1.0
[0.1.0]: https://gitlab.com/jramsay/automerge-bot/compare/v0.1.0...v0.0.2
[0.0.2]: https://gitlab.com/jramsay/automerge-bot/compare/v0.0.1...v0.0.2
[0.0.1]: https://gitlab.com/jramsay/automerge-bot/releases/tag/v0.0.1
