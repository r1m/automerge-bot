# Automerge Bot: automatic merges between stable branches

> **Warning:** it is not recommended to fix issues in old releases, then port fixes forward. It is **best practice** for all features and fixes to be merged first into the default branch, `master`, and then backported to previous releases.

Automerge Bot has been written for GitLab CI as a proof of concept, replicating the Bitbucket automerge feature as a stop gap for teams migrating from Bitbucket to GitLab.

## Usage

Using GitLab CI, the following **CI variables** are required/supported:

| **Environment variable**         | **Description**                                                                         |
|----------------------------------|-----------------------------------------------------------------------------------------|
| `GITLAB_ACCESS_TOKEN`            | Required. Used to create branches and merge requests.                                   |
| `AUTOMERGE_STABLE_BRANCH_PREFIX` | Optional. Default `release/`. Other branches will be ignored.                           |
| `AUTOMERGE_DEFAULT_BRANCH`       | Optional. Default `master`. The default branch is treated as the highest stable branch. |

Add the following job to your `.gitlab-ci.yml`:

```yaml
stages:
  - pre-build

auto_merge:
  stage: pre-build
  image: registry.gitlab.com/jramsay/automerge-bot:0.3.0
  script:
    - automerge
  only:
    refs:
      - /^release\/.*/  # only run the job on stable branches
  variables:
    GIT_STRATEGY: none # skip cloning since we'll use the API to create the merge request
    AUTOMERGE_STABLE_BRANCH_PREFIX: release/
    AUTOMERGE_DEFAULT_BRANCH: master
    AUTOMERGE_TO_DEFAULT_BRANCH: "false"
```

## How it works

Whenever new commits are pushed to a stable branch (e.g. a branch beginning with the `AUTOMERGE_STABLE_BRANCH_PREFIX`), the bot will:

- locate the merge request that introduced the commit
- create a merge request with the change targeting the next highest stable branch according to semantic versioning
- automatically approve and merge the merge request

If a merge request triggered the automerge chain, the author of the original merge request will be assigned to all automatically generated merge requests, so that they are notified of conflicts or pipeline failures.
