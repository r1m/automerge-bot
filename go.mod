module automerge-bot

require (
	github.com/andreyvit/diff v0.0.0-20170406064948-c7f18ee00883
	github.com/hashicorp/go-version v1.1.0
	github.com/sergi/go-diff v1.0.0 // indirect
	github.com/xanzy/go-gitlab v0.31.0
	golang.org/x/net v0.0.0-20190503192946-f4e77d36d62c // indirect
	golang.org/x/sync v0.0.0-20190423024810-112230192c58 // indirect
)

go 1.13
