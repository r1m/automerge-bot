package main

import (
	"fmt"
	"sort"
	"strings"

	"github.com/hashicorp/go-version"
	"github.com/xanzy/go-gitlab"
)

// ListStableBranches returns a list of sorted semver branches
func ListStableBranches(client *gitlab.Client, projectID string, stableBranchPrefix string) ([]string, error) {
	// Slice of semver branches
	vs := []struct {
		Branch  string
		Version *version.Version
	}{}

	// Scan all repository branches
	search := fmt.Sprintf("^%s", stableBranchPrefix)
	opt := &gitlab.ListBranchesOptions{
		ListOptions: gitlab.ListOptions{
			Page:    1,
			PerPage: 100,
		},
		Search: &search,
	}
	for {
		bs, resp, err := client.Branches.ListBranches(projectID, opt, nil)
		if err != nil {
			return nil, err
		}

		for _, b := range bs {
			if strings.HasPrefix(b.Name, stableBranchPrefix) == false {
				continue
			}

			ver, err := version.NewVersion(strings.TrimPrefix(b.Name, stableBranchPrefix))
			if err != nil {
				fmt.Printf("WARNING: could not parse stable branch version for '%s'\n", b.Name)
				continue
			}

			vs = append(vs, struct {
				Branch  string
				Version *version.Version
			}{
				Branch:  b.Name,
				Version: ver,
			})
		}

		if resp.CurrentPage >= resp.TotalPages {
			break
		}

		opt.Page = resp.NextPage
	}

	// Sort versions in ascending order (lowest version first)
	sort.Slice(vs, func(i, j int) bool { return vs[i].Version.LessThan(vs[j].Version) })

	branches := []string{}
	for _, v := range vs {
		branches = append(branches, v.Branch)
	}

	return branches, nil
}

// BranchIsSemver returns true if the stable branch prefix is followed by a
// valid semver version
func BranchIsSemver(branch string, stableBranchPrefix string) bool {
	_, err := version.NewVersion(strings.TrimPrefix(branch, stableBranchPrefix))
	if err != nil {
		return false
	}

	return true
}

// NextStableBranch returns the next highest stable branch according to semver.
// Expects branches to be sorted in ascending order (low to high)
func NextStableBranch(currentBranch string, bs []string, defaultBranch string) string {
	memo := ""

	for _, b := range bs {
		if memo == currentBranch {
			return b
		}

		memo = b
	}

	return defaultBranch
}

// Creates a branch from the current ref that can then be used to merge changes
// into the target branch.
func createBranch(client *gitlab.Client, projectID string, branchName string, sourceRef string, sourceSha string, targetRef string) error {

	opt := &gitlab.CreateBranchOptions{
		Branch: &branchName,
		Ref:    &sourceRef,
	}

	b, _, err := client.Branches.CreateBranch(projectID, opt, nil)
	if err != nil {
		if strings.Contains(err.Error(), "Branch already exists") {
			return errBranchAlreadyExists
		}
		return err
	}

	// FIXME: using the ref rather than the commit is a race condition, but isn't
	// significant problem. Using the commit to create a branch isn't possible via
	// the API yet. TODO: issues URL
	if b.Commit.ID != sourceSha {
		return errBranchRaceCondition
	}

	return nil
}
