/*
Automatic merges for GitLab

Automated merges from stable/maintenance branches to master using GitLab CI to
create the merge request.
*/

package main

import (
	"errors"
	"fmt"
	"os"

	"github.com/xanzy/go-gitlab"
)

var (
	// Automerge configuration variables
	botAccessToken    = "GITLAB_ACCESS_TOKEN"
	botDefaultBranch  = "AUTOMERGE_DEFAULT_BRANCH"
	botSemverPrefix   = "AUTOMERGE_STABLE_BRANCH_PREFIX"
	botMergeToDefault = "AUTOMERGE_TO_DEFAULT_BRANCH"

	// GitLab CI environment variables
	ciBaseURL   = "CI_API_V4_URL"
	ciProjectID = "CI_PROJECT_ID"
	ciBranch    = "CI_COMMIT_REF_NAME"
	ciCommit    = "CI_COMMIT_SHA"
)

var (
	errBranchAlreadyExists = errors.New("branch already exists")
	errBranchRaceCondition = errors.New("branch race condition detected")
	errOctopusMerge        = errors.New("octopus merge detected")
)

// AutoMerge contains all information needed to create automated merge
type AutoMerge struct {
	sourceBranch        string // release/0.0.2
	sourceSHA           string
	targetBranch        string // release/0.1.0
	tempBranch          string // automerge/123/release/0.1.0
	triggerMergeRequest *gitlab.MergeRequest
	triggerCommit       *gitlab.Commit
}

type Options struct {
	projectID      string
	sourceBranch   string
	sourceSHA      string
	defaultBranch  string
	semverPrefix   string
	mergeToDefault bool
}

func main() {
	var (
		// GitLab CI environment variables
		apiURL = os.Getenv(ciBaseURL)

		// Required environment variables
		accessToken = os.Getenv(botAccessToken)
	)

	opts := Options{
		// GitLab CI environment variables
		projectID:    os.Getenv(ciProjectID),
		sourceBranch: os.Getenv(ciBranch),
		sourceSHA:    os.Getenv(ciCommit),

		// Required environment variables
		semverPrefix: os.Getenv(botSemverPrefix),

		// Optional environment variables
		defaultBranch:  os.Getenv(botDefaultBranch),
		mergeToDefault: os.Getenv(botMergeToDefault) != "false",
	}

	if apiURL == "" {
		fmt.Println("Error: expected GitLab CI environment variables to be set")
		os.Exit(1)
	}

	if accessToken == "" {
		fmt.Printf("Error: expected %s to be set. API access is required to create new branches and merge requests", botAccessToken)
		os.Exit(1)
	}

	if opts.defaultBranch == "" {
		fmt.Printf("%s not set. Using 'master'\n", botDefaultBranch)
		opts.defaultBranch = "master"
	}

	if opts.semverPrefix == "" {
		fmt.Printf("%s not set. Using 'release/'\n", botSemverPrefix)
		opts.semverPrefix = "release/"
	}

	if opts.sourceBranch == opts.defaultBranch {
		fmt.Printf("Automerge has arrived at its destination: %s\n", opts.defaultBranch)
		return
	}

	if BranchIsSemver(opts.sourceBranch, opts.semverPrefix) == false {
		fmt.Printf("Error: expected a stable branch with prefix '%s', got %s\n", opts.semverPrefix, opts.sourceBranch)
		os.Exit(1)
	}

	client, err := gitlab.NewClient(accessToken, gitlab.WithBaseURL(apiURL))
	if err != nil {
		fmt.Printf("Error: %s", err)
		os.Exit(1)
	}

	// Read-only preparations for creating the automated merge request
	merge, err := prepareAutoMerge(client, opts)
	// projectID, semverPrefix, sourceBranch, sourceCommitID, defaultBranch, mergeToDefault)
	if err != nil {
		fmt.Printf("Error: %s", err)
		os.Exit(1)
	}

	// Write operations to create the branch, merge request and merge
	err = merge.createAutoMergeRequest(client, opts.projectID)
	if err != nil {
		fmt.Printf("Error: %s", err)
		os.Exit(1)
	}

}

func prepareAutoMerge(client *gitlab.Client, opts Options) (*AutoMerge, error) {
	triggerMergeRequest, triggerCommit, err := FindTrigger(client, opts.projectID, opts.sourceBranch, opts.sourceSHA)
	if err != nil {
		return nil, err
	}

	if triggerMergeRequest != nil {
		fmt.Printf("Automerge cascading from merge request !%d\n    %s\n", triggerMergeRequest.IID, triggerMergeRequest.WebURL)
	}

	if triggerCommit != nil {
		fmt.Printf("Automerge cascading from commit %s\n", triggerCommit.ID)
	}

	stableBranches, err := ListStableBranches(client, opts.projectID, opts.semverPrefix)
	if err != nil {
		return nil, err
	}

	targetBranch := NextStableBranch(opts.sourceBranch, stableBranches, opts.defaultBranch)
	fmt.Printf("Next stable semver branch is '%s'\n", targetBranch)

	if opts.mergeToDefault == false && targetBranch == opts.defaultBranch {
		fmt.Printf("Automerge has arrived at its destination: %s\n", opts.defaultBranch)
		os.Exit(0)
	}

	tempBranch := ""
	if triggerMergeRequest != nil {
		tempBranch = fmt.Sprintf("automerge/%d/%s", triggerMergeRequest.IID, targetBranch)
	}

	if triggerCommit != nil {
		tempBranch = fmt.Sprintf("automerge/%s/%s", triggerCommit.ShortID, targetBranch)
	}

	merge := &AutoMerge{
		sourceBranch:        opts.sourceBranch,
		sourceSHA:           opts.sourceSHA,
		targetBranch:        targetBranch,
		tempBranch:          tempBranch,
		triggerMergeRequest: triggerMergeRequest,
		triggerCommit:       triggerCommit,
	}

	return merge, nil

}

func (merge AutoMerge) createAutoMergeRequest(client *gitlab.Client, projectID string) error {
	err := createBranch(client, projectID, merge.tempBranch, merge.sourceBranch, merge.sourceSHA, merge.targetBranch)
	if err != nil {
		if err != errBranchAlreadyExists {
			return err
		}

		fmt.Printf("Temporary branch '%s' already exists...\n", merge.tempBranch)
	} else {
		fmt.Printf("Temporary branch '%s' created...\n", merge.tempBranch)
	}

	title, description, err := merge.MarshalMemo()
	if err != nil {
		return err
	}

	assignee := 0
	if merge.triggerMergeRequest != nil {
		assignee = merge.triggerMergeRequest.Author.ID
	}

	mr, err := createMergeRequest(client, projectID, merge.tempBranch, merge.targetBranch, title, description, assignee)
	if err != nil {
		return err
	}

	if err := acceptMergeRequest(client, projectID, mr); err != nil {
		fmt.Printf("Automatic merge failed.\n\nTroubleshoot by checking for:\n - merge conflicts\n - required approvals\n")
		os.Exit(0)
	}

	return nil
}

// GetCommit returns the commit resource for the given sha
func GetCommit(client *gitlab.Client, projectID string, sha string) (*gitlab.Commit, error) {
	commit, _, err := client.Commits.GetCommit(projectID, sha, nil)
	if err != nil {
		return nil, err
	}

	return commit, nil
}

// GetMergeRequest returns the merge request resource for the given merge request
func GetMergeRequest(client *gitlab.Client, projectID string, iid int) (*gitlab.MergeRequest, error) {
	mr, _, err := client.MergeRequests.GetMergeRequest(projectID, iid, nil)
	if err != nil {
		return nil, err
	}

	return mr, nil
}

// GetMergeRequests returns the merge request resources for the given SHA
func GetMergeRequests(client *gitlab.Client, projectID string, sha string) ([]*gitlab.MergeRequest, error) {
	mrs, _, err := client.Commits.GetMergeRequestsByCommit(projectID, sha, nil)
	if err != nil {
		return nil, err
	}

	return mrs, nil
}

// FindTrigger returns either the merge request or commit that triggered the
// cascading automerge.
func FindTrigger(client *gitlab.Client, projectID string, sourceBranch string, sourceSHA string) (*gitlab.MergeRequest, *gitlab.Commit, error) {
	var (
		triggerMergeRequest *gitlab.MergeRequest
		triggerCommit       *gitlab.Commit
		sourceMergeRequest  *gitlab.MergeRequest
		sourceCommit        *gitlab.Commit
	)

	sourceCommit, err := GetCommit(client, projectID, sourceSHA)
	if err != nil {
		return nil, nil, err
	}

	// GitLab only creates merge commits with two parents, therefore the source
	// commit is the commit that triggered the cascading automerge
	if len(sourceCommit.ParentIDs) != 2 {
		triggerCommit = sourceCommit
		return nil, triggerCommit, nil
	}

	// Left commit comes from the target branch, right from the source branch of
	// the merge request we are trying to locate
	parentSHA := sourceCommit.ParentIDs[1]

	// Find the merge request containing this commit
	relatedMergeRequests, err := GetMergeRequests(client, projectID, parentSHA)
	if err != nil {
		return nil, nil, err
	}
	for _, mr := range relatedMergeRequests {
		if mr.State == "merged" && mr.SHA == parentSHA && mr.MergeCommitSHA == sourceSHA {
			sourceMergeRequest = mr
			break
		}
	}

	// No merge requests found means a merge commit push manually. Therefore this
	// is the commit that triggered the cascading automerge.
	if sourceMergeRequest == nil {
		triggerCommit = sourceCommit
		return nil, triggerCommit, nil
	}

	// Read from source merge request description
	triggerMergeRequest, triggerCommit, err = UnmarshalMemo(client, projectID, sourceMergeRequest.Description)
	if err != nil {
		return nil, triggerCommit, nil
	}

	if triggerMergeRequest == nil && triggerCommit == nil {
		triggerMergeRequest = sourceMergeRequest
		return triggerMergeRequest, nil, nil
	}

	if triggerMergeRequest != nil && triggerCommit != nil {
		return nil, nil, fmt.Errorf("epected trigger merge request or commit, not both")
	}

	return triggerMergeRequest, triggerCommit, nil
}

func createMergeRequest(client *gitlab.Client, projectID string, sourceBranch string, targetBranch string, title string, description string, assigneeID int) (*gitlab.MergeRequest, error) {
	removeSourceBranch := true

	opt := &gitlab.CreateMergeRequestOptions{
		Title:              &title,
		Description:        &description,
		SourceBranch:       &sourceBranch,
		TargetBranch:       &targetBranch,
		RemoveSourceBranch: &removeSourceBranch,
	}

	if assigneeID != 0 {
		opt.AssigneeID = &assigneeID
	}

	mr, _, err := client.MergeRequests.CreateMergeRequest(projectID, opt, nil)
	if err != nil {
		return nil, err
	}

	return mr, nil
}

func acceptMergeRequest(client *gitlab.Client, projectID string, mr *gitlab.MergeRequest) error {
	if _, _, err := client.MergeRequestApprovals.ApproveMergeRequest(projectID, mr.IID, nil); err != nil {
		return err
	}

	mergeWhenPipelineSucceeds := true
	removeSourceBranch := true
	opt := &gitlab.AcceptMergeRequestOptions{
		MergeWhenPipelineSucceeds: &mergeWhenPipelineSucceeds,
		ShouldRemoveSourceBranch:  &removeSourceBranch,
	}

	if _, _, err := client.MergeRequests.AcceptMergeRequest(projectID, mr.IID, opt, nil); err != nil {
		// Workaround for https://gitlab.com/gitlab-org/gitlab/-/issues/217710
		fmt.Println("Merge failed. Retrying without MWPS...")
		// return err
	}

	mergeWhenPipelineSucceeds = false
	if _, _, err := client.MergeRequests.AcceptMergeRequest(projectID, mr.IID, opt, nil); err != nil {
		return err
	}

	fmt.Println("Merge suceeded.")

	return nil
}
